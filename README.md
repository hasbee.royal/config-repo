1. Download AWS Corretto 11 windowsx64 ZIP version from below download page.
https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html
Direct download link: 
https://d3pxv6yz143wms.cloudfront.net/11.0.4.11.1/amazon-corretto-11.0.4.11.1-windows-x64.zip

2. Download latest IntelliJ IDE, if you dont have.
3. Create and empty project, and export all modules in side TCP-POC directory one by one as gradle project - For easiness sake
4. If the IntelliJ IDE is latest, add environment variables below for windows command line option in the "Terminal", change directory accordingly
   set JAVA_HOME=C:\projects\Software\amazon-corretto-11.0.12.7.1-windows-x64-jdk\jdk11.0.12_7
   set PATH=C:\projects\Software\amazon-corretto-11.0.12.7.1-windows-x64-jdk\jdk11.0.12_7\bin
5. Update configserver/application.yml, config.searchLocation property with the root repository copied path. (Just before services directory)
6. gradlew clean build -x test, build each module one by one
7. Start config server: 
   java -jar {service}\build\libs\{service_name}.jar

8. Start eurekaserver: 
9. Start gateway:
   java -jar {service}\build\libs\{service_name}.jar --spring.profiles.active=local
10. Start userservice
11. check Eureka GUI: localhost:6002
12. Check gateway routing: localhost:6001/gateway/userservice/greeting

gradlew clean build -x test 
