package com.mtnsa.gateway.config.proxy;

import com.mtnsa.gateway.config.auth.SessionFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.server.WebSession;
import reactor.core.publisher.Mono;

@Configuration
public class RouteConfig {

    Logger logger = LoggerFactory.getLogger(RouteConfig.class);
    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        logger.info("inside gateway routes");
        return builder.routes()
                .route("userservice", r -> r
                        .path("/userservice/**")
                        .filters(f -> f.stripPrefix(1))
                        .uri("lb://userservice")
                ).route("userservice2", r -> r
                        .path("/gateway/userservice/**")
                        .filters(f -> f.stripPrefix(2))
                        .uri("lb://userservice")
                )
                .build();
    }
}
