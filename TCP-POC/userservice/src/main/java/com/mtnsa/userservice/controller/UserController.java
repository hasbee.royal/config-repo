package com.mtnsa.userservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Value("${my.greeting}")
    private String greetingMessage;

    @Value("some test data")
    private String staticMessage;

    @Value("${my.list.greetings}")
    private List<String> listOfMessages;

    @GetMapping("/greeting")
    public String greeting() {
        return greetingMessage + staticMessage + listOfMessages;
    }
}
